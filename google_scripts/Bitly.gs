/*====================================================================================================================================*
  BitlyClicks by Shiva Velingker
  ====================================================================================================================================
  Determines the number of clicks per Bitly link.
  Fetching the Bitly URL returns a bunch of junk plus some of the actually relevant numbers.
  Regex is used to parse out the relevant number.
*/
function BitlyClicks(url){
  var rawdata = UrlFetchApp.fetch(url, null);
  var regExp = new RegExp(/user_clicks": (\d+)/g);
  var clicks = regExp.exec(rawdata.getContentText())[1];
  return clicks;
}