/*====================================================================================================================================*
  resetDeltas by Shiva Velingker
  ====================================================================================================================================
  Goes through every rider, gets their current fundraising amount, and
  hard codes it into a formula to track the dynamic difference over time.
  
  Ex: If RiderA has $500 this week, it creates a formula (currentAmount - 500)
  so that we can track the amount a rider makes over a given time period.
*/

function resetDeltas() {
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("RawData");
  
  // Save Fundraising Amounts
  for(var i = 3; i <= 88; i++){
  
    // Save Fundraising
    var prevMoney = sheet.getRange('D'+i).getValue();
    var differenceFormula = "D"+i+ " - "+ prevMoney;
    sheet.getRange("E"+i).setFormula(differenceFormula);
    
    // Save Bitly
    var prevClicks = sheet.getRange('H'+i).getValue() || 0;
    differenceFormula = "H"+i+ " - "+ prevClicks;
    sheet.getRange("I"+i).setFormula(differenceFormula);
  }
  
  // Update timestamps
  var now = (new Date()).toLocaleString();
  sheet.getRange('E2').setValue('Since ' + now);
  sheet.getRange('I2').setValue('Since ' + now);
}
