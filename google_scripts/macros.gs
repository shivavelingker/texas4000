/*====================================================================================================================================*
  Refresh by Shiva Velingker
  ====================================================================================================================================
  Google doesn't call a function again unless it's value changes.
  Each EverydayHero and Bitly link are concatenated with a single string. That single string is changed to force a reset of all links.
  
  EverydayHero links all have ".json" on the end, so this is removed and re-added to force a refresh.
  Bitly links are normal, so they have a junk value added and then removed.
*/
function Refresh() {
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("RawData");
  
  // Force EverydayHero links to refresh
  sheet.getRange('K3').clear({contentsOnly: true, skipFilteredRows: true});
  sheet.getRange('K3').setValue('.json');
  
  // Force Bitly links to refresh
  sheet.getRange('K4').setValue('crap');
  sheet.getRange('K4').clear({contentsOnly: true, skipFilteredRows: true});
  
  var now = new Date();
  sheet.getRange('D1').setValue('Updated @ ' + now.toLocaleString());
};

function ResetRiderDifferences() {
  resetDeltas();
};