# Texas 4000
Scripts to assist with web-scraping data for Texas 4000.

The Python script is run hourly from a UTCS computer.

The Google scripts are linked to the Google Sheet (as a project under `Tools > Script Editor`). The Refresh macro is run every hour from Google App Scripts' Project Trigger (`Edit > Current project's triggers`).

Google scripts are only used for the Fundraising board. All other changes come from the Python script (i.e., RMS) and functions intrinsic to the Google sheet that pull data from other sheets.