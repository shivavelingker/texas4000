'''
	Goal: Consolidate all Google Sheets by figuring out each rider's ROW on each doc.

	For example, Fundraising may refer to "Frank Garza" as "Frank Garza", but RMS may refer to him as "Francisco Garza".
	This script utilizes "unique identifiers" for each rider (to resolve duplicate first or last names).
	It determines which row on each Google Sheet the rider has information on.
	It then saves all this to the Dashboard. The Dashboard then uses these rows to pull data from each Sheet directly.

	Script can take a long time to run due to have to pull each row from each doc. Probably faster to pull rows in batch.
	NB: "row" = "idx"
'''

import pygsheets

SHEETS = {
	"DASHBOARD": "1pAwVd0Jk_l8lhjbfJ20RUAxNMywfVysmfyeFD--La88",
	"FUNDRAISING": "1KUV7gvz_eRVJEvBVnM3iNR8vgkDT-Dkr_meDpRaAX7Y",
	"SAG": "1xIh0efoAgGClxl0o57se8ogMSuX6kQ_9kT-NfLuc2P8",
	"VOLUNTEERING": "1ioa_6V2LYoWVQ6ECyOhVGvCzEarfniYpVoh91ys-rsA",
	"DOCUMENTS": "1CC195Eg0RrfNxiWfF_ACv6de6lKzkh8sIkuqkH-xkf0",
	"KITORDER": "1RRQDyNdb0GlW1le0Z1i-e_tjdfPUVfbdU350xsaFdNM",
	"ATTENDANCE": "1ZQxlH1bLAgXPEBJ9wYT93XzeNyDeZT90qeXUochKol8"
}

gc = pygsheets.authorize()
riderKeys = {}

class Rider:
	def __init__(self, **kwargs):
		self.key = kwargs.get("key", None)
		self.idx = kwargs.get("idx", None)

		self.fundraising = kwargs.get("fundraising", None)
		self.rms = kwargs.get("rms", None)
		self.sag = kwargs.get("sag", None)
		self.volunteering = kwargs.get("volunteering", None)
		self.documents = kwargs.get("documents", None)
		self.kit = kwargs.get("kit", None)
		self.attendance = kwargs.get("attendance", None)

	def _data(self):
		yield self.fundraising or ""
		yield self.rms or ""
		yield self.sag or ""
		yield self.volunteering or ""
		yield self.documents or ""
		yield self.kit or ""
		yield self.attendance or ""

	'''
		Google Sheets requires a matrix
	'''
	def data(self):
		return [ list(self._data()) ]


'''
	Each rider can be identified by a unique part of their name.

	Ex: Caroline Bik and Caroline Weaver can be identified by "Bik" and "Weaver"
	Ex: Tonya Chen and Michelle Chen can be identified by "Tonya" and "Michelle"

	This function gets those unique keys
'''
def getKeys():
	spreadsheet = gc.open_by_key(SHEETS["DASHBOARD"])
	sheet = spreadsheet.worksheet_by_title("Indices")

	for idx in range(2, sheet.rows):
		row = sheet.get_row(idx)

		if not row[0]:
			break

		properties = {
			"idx": idx,
			"key": row[2],
			"fundraising": row[3],
			"rms": row[4],
			"sag": row[5],
			"volunteering": row[6],
			"documents": row[7],
			"kit": row[8],
			"attendance": row[9]
		}
		rider = Rider(**properties)
		riderKeys[rider.key] = rider


'''
	Searches for a rider's unique identifier within a given string to see if the rider is equivalent to the provided string
'''
def matchRider(rider):
	for uniqKey in riderKeys:
		if uniqKey.lower() in rider.lower():
			return uniqKey


def getFundraising():
	spreadsheet = gc.open_by_key(SHEETS["FUNDRAISING"])
	sheet = spreadsheet.worksheet_by_title("RawData")

	for idx in range(3, sheet.rows):
		row = sheet.get_row(idx)

		if not row[0]:
			break

		fullName = ' '.join(row[1:3])
		riderKey = matchRider(fullName)

		if not riderKey:
			print("Failed to identify rider: %s" % fullName)
			continue
			
		riderKeys[riderKey].fundraising = idx


def getMiles():
	spreadsheet = gc.open_by_key(SHEETS["DASHBOARD"])
	sheet = spreadsheet.worksheet_by_title("RMS")

	for idx in range(2, sheet.rows):
		row = sheet.get_row(idx)

		if not row[0]:
			break

		fullName = row[0]
		riderKey = matchRider(fullName)

		if not riderKey:
			print("Failed to identify rider: %s" % fullName)
			continue
			
		riderKeys[riderKey].rms = idx


def getSAG():
	spreadsheet = gc.open_by_key(SHEETS["SAG"])
	sheet = spreadsheet.worksheet_by_title("Tracking Sheet")

	for idx in range(3, sheet.rows):
		row = sheet.get_row(idx)

		if not row[0]:
			break

		fullName = row[0] # This doc has last name, first but order doesn't matter
		riderKey = matchRider(fullName)

		if not riderKey:
			print("Failed to identify rider: %s" % fullName)
			continue
			
		riderKeys[riderKey].sag = idx

def getVolunteering():
	spreadsheet = gc.open_by_key(SHEETS["VOLUNTEERING"])
	sheet = spreadsheet.worksheet_by_title("Hours Log")

	for idx in range(2, sheet.rows):
		row = sheet.get_row(idx)

		if not row[0]:
			break

		fullName = ' '.join(row[0:2]) # This doc has last name, first but order doesn't matter
		riderKey = matchRider(fullName)

		if not riderKey:
			print("Failed to identify rider: %s" % fullName)
			continue
			
		riderKeys[riderKey].volunteering = idx


def getDocuments():
	spreadsheet = gc.open_by_key(SHEETS["DOCUMENTS"])
	sheet = spreadsheet.worksheet_by_title("Rider Documents")

	for idx in range(2, sheet.rows):
		row = sheet.get_row(idx)

		if not row[0]:
			break

		fullName = row[0]
		riderKey = matchRider(fullName)

		if not riderKey:
			print("Failed to identify rider: %s" % fullName)
			continue
			
		riderKeys[riderKey].documents = idx


def getKitMoney():
	spreadsheet = gc.open_by_key(SHEETS["KITORDER"])
	sheet = spreadsheet.worksheet_by_title("Form Responses 1")

	for idx in range(3, sheet.rows):
		row = sheet.get_row(idx)

		if not row[0]:
			break

		fullName = ' '.join(row[0:2])
		riderKey = matchRider(fullName)

		if not riderKey:
			print("Failed to identify rider: %s" % fullName)
			continue
			
		riderKeys[riderKey].kit = idx


def getAttendance():
	spreadsheet = gc.open_by_key(SHEETS["ATTENDANCE"])
	sheet = spreadsheet.worksheet_by_title("2019 Spring Team Meeting Attendance")

	for idx in range(4, sheet.rows):
		row = sheet.get_row(idx)

		if not row[1]:
			break

		fullName = ' '.join(row[0:2])
		riderKey = matchRider(fullName)

		if not riderKey:
			print("Failed to identify rider: %s" % fullName)
			continue
			
		riderKeys[riderKey].attendance = idx


def saveData():
	spreadsheet = gc.open_by_key(SHEETS["DASHBOARD"])
	sheet = spreadsheet.worksheet_by_title("Indices")

	startCol = "D"
	numOfCols = len(riderKeys[ matchRider("Shiva") ].data()[0] )
	def getRange(idx):
		yield startCol + str(idx)
		yield chr( ord(startCol) + numOfCols - 1 ) + str(idx)

	for rider in riderKeys.values():
		updateRange = ':'.join(getRange(rider.idx))
		sheet.update_values(updateRange, rider.data())


if __name__ == '__main__':
	print("Getting metadata")
	getKeys()

	print("Parsing Fundraising doc")
	getFundraising()

	print("Parsing RMS doc")
	getMiles()

	print("Parsing SAG doc")
	getSAG()

	print("Parsing Volunteering doc")
	getVolunteering()

	print("Parsing F&O Documents doc")
	getDocuments()

	print("Parsing F&O Kit Order doc")
	getKitMoney()

	print("Parsing F&O Meeting Attendance doc")
	getAttendance()

	print("All data gathered!")

	print("Uploading to Dashboard")
	saveData()
