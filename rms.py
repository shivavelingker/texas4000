import requests
from lxml import html
import pygsheets
import datetime

sortedRiders = []

def isFloat(num):
    try:
        float(num)
        return True
    except:
        return False

def scrapeRms():
    USERNAME = "svelingker@utexas.edu"
    PASSWORD = "svelingker@utexas.edu"

    LOGIN_URL = "http://rms.texas4000.org/login/"
    URL = "http://rms.texas4000.org/team/stats/"

    session_requests = requests.session()

    # Get CSRF token
    result = session_requests.get(LOGIN_URL)
    tree = html.fromstring(result.text)
    CSRF = list(set(tree.xpath("//input[@name='csrfmiddlewaretoken']/@value")))[0]

    # Create login payload
    payload = {
        "username": USERNAME, 
        "password": PASSWORD, 
        "csrfmiddlewaretoken": CSRF
    }

    # Login
    result = session_requests.post(LOGIN_URL, data = payload, headers = dict(referer = LOGIN_URL))

    # Scrape leaderboard
    result = session_requests.get(URL, headers = dict(referer = URL))
    tree = html.fromstring(result.content)

    # Interpret HTML
    riders = {}
    for row in tree.iter("tr"):
        name = row[0].text
        miles = row[1].text

        if isFloat(miles):
            riders[name] = miles

    # Sort data alphabetically
    for rider in sorted(riders):
        sortedRiders.append([rider, riders[rider]])

def uploadToGoogle():
    # Get sheet
    gc = pygsheets.authorize()
    spreadsheet = gc.open_by_key("1pAwVd0Jk_l8lhjbfJ20RUAxNMywfVysmfyeFD--La88")
    rms = spreadsheet.worksheet_by_title("RMS")

    # Get range; necessary to do each time in case a rider has dropped
    size = 0
    for idx in range(1, rms.rows):
        row = rms.get_row(idx)

        if not row[0]:
            break

        size += 1
    sheetRange = "A2:B" + str(size + 1)

    # If a rider has dropped, old data won't get overwritten. Overwrite now
    for blank in range(len(sortedRiders), size):
        sortedRiders.append(["", ""])

    # Write to sheet
    cells = rms.range(sheetRange, returnas='range')
    cells.update_values(sortedRiders)

    now = datetime.datetime.now()
    updateString = now.strftime('Updated @ %b %d, %Y %I:%M %p')
    rms.update_value('C1', updateString)    
    
if __name__ == '__main__':
    scrapeRms()
    uploadToGoogle()
